★ [[doc/FAQs.org][← FAQs overview page]] ★

* What about this strange name and logo?

[[file:../../logo/Orgdown%20logo%20-%2064x64%20transparent%20background.png]]

** Orgdown Name and Orgdown Logo O↓

The name "Orgdown" can be considered as a logical consequence when
Org-mode meets the idea of other simplified markup languages:

=Org-mode=  -simplify→   =Org down= (this spelling is not used here)  -merge→  =Orgdown=  -simplify→  =OD=  -coolify→  =O↓=

*** Bonus question: Why not use the name "..."?

- "Org": The term "org" is used all the time as a short version of
  Org-mode. Therefore, it would not have helped to differ between
  syntax and implementation.
- "Orgup": Karl preferred recognition ("Mark-down"). Furthermore, Karl
  thought that Orgdown is something "less" than Org-mode and therefore
  "down" is the term that reflects this much better.
- "...": Yes, there are more clever names out there. Let's concentrate
  on the goal and not the name. ;-)

** Preferred names

The preference for the level Orgdown1 name is:

1. Orgdown1  ... if you have the room for the long form
2. O↓1  ... of you know how to enter the downward arrrow characters
3. OD1  ... this is the easiest to type short form

For "Orgdown", you just need to omit "1" accordingly.

** Logo: O↓

[[https://gitlab.com/publicvoit/gitlab-tests/-/tree/main/logo][The logo files]] created by [[https://Karl-Voit.at][Karl Voit]] are licensed under a [[https://en.wikipedia.org/wiki/Creative_Commons_license][Creative Commons]] license [[https://creativecommons.org/share-your-work/public-domain/cc0/][CC0 “No Rights Reserved”]] to guarantee a maximum level of freedom:

- [[file:../../logo/Orgdown%20logo.svg][Orgdown logo.svg]] (Original; best quality)
- [[file:../../logo/Orgdown%20logo%20-%20400x400%20white%20background.png][Orgdown logo - 400x400 white background.png]]
- [[file:../../logo/Orgdown%20logo%20-%20400x400%20transparent%20background.png][Orgdown logo - 400x400 transparent background.png]]
- [[file:../../logo/Orgdown%20logo%20-%20200x200%20white%20background.png][Orgdown logo - 200x200 white background.png]]
- [[file:../../logo/Orgdown%20logo%20-%20200x200%20transparent%20background.png][Orgdown logo - 200x200 transparent background.png]]
- [[file:../../logo/Orgdown%20logo%20-%2064x64%20white%20background.png][Orgdown logo - 64x64 white background.png]]
- [[file:../../logo/Orgdown%20logo%20-%2064x64%20transparent%20background.png][Orgdown logo - 64x64 transparent background.png]]
- [[file:../../logo/Orgdown%20logo%20-%2032x32%20white%20background.png][Orgdown logo - 32x32 white background.png]]
- [[file:../../logo/Orgdown%20logo%20-%2032x32%20transparent%20background.png][Orgdown logo - 32x32 transparent background.png]]
- [[file:../../logo/Orgdown%20logo%20-%2032x32%20white%20background.ico][Orgdown logo - 32x32 white background.ico]]
 

---------------

★ [[doc/FAQs.org][← FAQs overview page]] ★
