★ [[Overview.org][← Overview page]] ★

* Orgdown Levels

Orgdown is the common term that describes a set of well-defined
sub-sets of [[Org-mode.org][Org-mode]]. In order to provide different levels of support,
Orgdown splits up into different levels.

* Orgdown1

There is currently one initial Orgdown level named *Orgdown1*.
It covers [[Orgdown1-Syntax-Examples.org][the most basic set of syntax elements]] that any tool has to
support in order to support Orgdown.

Orgdown1 < Orgdown2 < ... ≪ Org-mode

Multiple levels of Orgdown with more syntax elements [[Roadmap.org][might be defined
in the future]] if Orgdown1 is getting more and more popular. A future
Orgdown2 level contains everything of Orgdown1 and an additional set
of syntax elements derived from Org-mode. A hypothetical highest level
of Orgdown would be Org-mode itself.

- OD1 → already published and documented here
  - OD1 is easy to comply to and comes with a very vague definition
    for tools. It's a door-opener to the world of Orgdown with a low entry barrier.
- OD2 → will be defined in future
- OD3 → will be defined in future
- ...
- OD∞ = Org-mode (by definition)

Levels higher than OD1 might as well get a more formalized definition.
It is unclear if there will be any ODx-verifier for Orgdown files
rather than a compatibility checker for Orgdown support in a specific
tool or service.

** OD1 Compatibility

The [[Tool-Support.org][Tool-Support page]] lists "OD1 Compatibility" percentage numbers for
each tool that has an assessment page of its own.

This percentage reflects a rough idea on the maturity level of OD1
support of a tool.

It is calculated on the detailed tool pages as following: OD1 is
defined by 43 single features (see [[tools/Template.org][that tool template]]) that can be checked. Each feature is
either supported with a basic level, which is reflected by the value 1.
Or the feature is supported in an advanced way, which is reflected by
the value 2. An advanced feature might relate with syntax highlighting
or even keyboard shortcuts that insert a new element of that type or
similar.

After all 43 features did get their values, the sum will be
calculated. This sum is then compared to the maximum value there is
(43*2=86). This way, we can come up with a percentage of OD1 support
as a rough indicator.

The aim for OD1 is similar to "any OD1 supporting tool should display
OD1 syntax elements and gracefully display everything that extends OD1
syntax".

Note that OD1 is defined as a rather vague/smooth set of rules to
enable a gracefully entranct to the world of Orgdown. Higher levels
might get defined much more formal.
